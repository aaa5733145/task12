﻿#include <iostream>
const int N = 20;

void FindOddNumbers(int Limit, bool IsOdd)
{

        for (int i = IsOdd; i < Limit; i+=2)
        {
                std::cout << i << "\n";
        }

}

int main()
{
    std::cout << "No Function \n";
    for (int i = 0; i < N; i++)
    {
        if (i % 2 == 0)
            std::cout << i << "\n";
    }

    std::cout << "With Function \n";
    FindOddNumbers(N,true);
}
